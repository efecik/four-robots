﻿using System;
using System.Collections.Generic;
using System.IO;

namespace FourRobots
{
    class MapHelper
    {
        public static double FindDistanceSquared((int x, int y) a, (int x, int y) b)
        {
            return Math.Pow(a.x - b.x, 2) + Math.Pow(a.y - b.y, 2);
        }

        public static bool IsBetween((int x, int y) testValue, (int x, int y) bound1, (int x, int y) bound2)
        {
            return (testValue.x >= Math.Min(bound1.x, bound2.x) && testValue.x <= Math.Max(bound1.x, bound2.x))
                && (testValue.y >= Math.Min(bound1.y, bound2.y) && testValue.y <= Math.Max(bound1.y, bound2.y));
        }
    }

    class Robot
    {
        // Corners of Operation Area, start being the closest to the origin
        (int x, int y) startCorner;
        public (int x, int y) StartCorner { get => startCorner; }
        (int x, int y) endCorner;
        public (int x, int y) EndCorner { get => endCorner; }

        (int x, int y) position;
        public (int x, int y) Position { get => position; set => position = value; }

        string name;
        public string Name { get => name; }

        // For Test
        int moveCounter;

        public Robot(int cornerX, int cornerY, int areaWidth, int areaHeigth, string name)
        {
            startCorner = startCorner = (cornerX, cornerY);
            endCorner = (cornerX == 0 ? areaWidth : cornerX - areaWidth, cornerY == 0 ? areaHeigth : cornerY - areaWidth);
            position = startCorner;
            this.name = name;
            moveCounter = 0;
        }

        public override string ToString()
        {
            return name + " moved " + moveCounter + " times.\n";
        }

        public void Move((int x, int y) point)
        {
            if (point.x != -1)
            {
                moveCounter++;
            }

            position = point;
        }

        public void reset()
        {
            position = startCorner;
        }
    }

    class RobotRunner
    {
        List<Robot> robots;
        List<(int, int)> visitPoints;
        int stepCount;

        public RobotRunner(List<Robot> robots, List<(int, int)> visitPoints)
        {
            this.robots = robots;
            this.visitPoints = visitPoints;
            stepCount = 0;
        }

        int FindIndexOfClosestPointAvailableToRobot(Robot r)
        {
            int closestPointIndex = -1;
            double closestDistanceSquared = float.MaxValue;

            for (int i = 0; i < visitPoints.Count; i++)
            {
                bool shouldSkip = false;
                // Discard points that are out of range and not in between center of mass and the starting point
                if (!MapHelper.IsBetween(visitPoints[i], r.StartCorner, r.EndCorner))
                {
                    shouldSkip = true;
                }

                // Discard points that are occupied by other Robots
                for (int j = 0; j < robots.Count && !shouldSkip; j++)
                {
                    if (r != robots[j] && MapHelper.IsBetween(visitPoints[i], robots[j].Position, robots[j].StartCorner))
                    {
                        shouldSkip = true;
                    }
                }

                if (!shouldSkip)
                {
                    double tempDistance = MapHelper.FindDistanceSquared(r.StartCorner, visitPoints[i]);
                    if (closestDistanceSquared > tempDistance)
                    {
                        closestDistanceSquared = tempDistance;
                        closestPointIndex = i;
                    }
                }
            }
            return closestPointIndex;
        }

        int FindFirstIndexOfAPointAvailableOnlyToRobot(Robot r)
        {
            bool shouldSkip = false;
            for (int i = 0; i < visitPoints.Count; shouldSkip=false,i++)
            {
                // Discard points that are out of range
                if(!MapHelper.IsBetween(visitPoints[i], r.StartCorner, r.EndCorner))
                    shouldSkip = true;

                // Discard points that are available to other Robots
                for (int j = 0; j < robots.Count && !shouldSkip; j++)
                {
                    if (r != robots[j] && MapHelper.IsBetween(visitPoints[i], robots[j].StartCorner, robots[j].EndCorner))
                    {
                        shouldSkip = true;
                    }
                }

                if(!shouldSkip)
                {
                    return i;
                }
            }
            return -1;
        }

        void moveRobotToAPoint(Robot r, int pointIndex)
        {
            r.Move(pointIndex != -1 ? visitPoints[pointIndex] : (-1, -1));
            Console.WriteLine(String.Format("{0}, {1}, {2}, {3}", stepCount, r.Name, r.Position.x, r.Position.y));
            if (pointIndex != -1)
                visitPoints.RemoveAt(pointIndex);
        }

        void resetRobots()
        {
            foreach (Robot r in robots)
            {
                r.reset();
            }
        }

        void Step()
        {
            stepCount++;
            resetRobots();
            foreach (Robot r in robots)
            {
                int indexOfPointOnlyAvailableToRobot = FindFirstIndexOfAPointAvailableOnlyToRobot(r);
                int nextPointIndex = indexOfPointOnlyAvailableToRobot != -1 ? indexOfPointOnlyAvailableToRobot : FindIndexOfClosestPointAvailableToRobot(r);
                moveRobotToAPoint(r, nextPointIndex);
            }
        }

        public void Run()
        {
            while (visitPoints.Count > 0)
                Step();
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            // Variables
            int sizeWidth;
            int sizeHeight;
            List<Robot> robots = new List<Robot>();
            List<(int, int)> visitPoints = new List<(int x, int y)>();

            // Parse File
            // if a path is given in args, use it; else use the default text
            string filePath = args.Length > 0 ? args[0] : @"C:\Users\sousp\source\repos\FourRobots\test.txt";
            // Read a text file line by line.  
            string[] lines = File.ReadAllLines(filePath);

            // Parse area
            string[] areaSize = lines[1].Split(", ");
            sizeWidth = int.Parse(areaSize[0]);
            sizeHeight = int.Parse(areaSize[1]);

            // Parse Robots
            (int x, int y)[] coordinates = { (0, 0), (0, sizeHeight), (sizeWidth, 0), (sizeWidth, sizeHeight) };

            for (int i = 0; i < 4; i++)
            {
                string[] robotWorkArea = lines[i + 3].Split(", ");
                robots.Add(new Robot(coordinates[i].x, coordinates[i].y, int.Parse(robotWorkArea[1]), int.Parse(robotWorkArea[2]), robotWorkArea[0]));
            }

            // Parse Visit Points
            for (int i = 8; i < lines.Length; i++)
            {
                string[] visitPoint = lines[i].Split(", ");
                visitPoints.Add((int.Parse(visitPoint[0]), int.Parse(visitPoint[1])));
            }

            RobotRunner rr = new RobotRunner(robots, visitPoints);

            rr.Run();

            // For Debug
            for (int i = 0; i < robots.Count; i++)
                Console.WriteLine(robots[i]);
        }
    }
}
